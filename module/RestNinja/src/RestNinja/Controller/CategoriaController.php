<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RestNinja\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;


class CategoriaController extends AbstractRestfulController
{
    public function getList()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $em->getRepository('Application\Entity\Categoria')->findAll();
        return $data;
    }

    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $em->getRepository('Application\Entity\Categoria')->find($id);
        return $data;
    }

    public function create($data)
    {
        $serviceCategoria = $this->getServiceLocator()->get('Application\Service\Categoria');
        $categoria = $serviceCategoria->insert($data);

        if ($categoria) {
            return $categoria;
        } else {
            return array('success' => false);
        }
    }

    public function update($id, $data)
    {
        $serviceCategoria = $this->getServiceLocator()->get('Application\Service\Categoria');
        $categoria = $serviceCategoria->update($data);

        if ($categoria) {
            return $categoria;
        } else {
            return array('success' => false);
        }
    }

    public function delete($id)
    {
        $serviceCategoria = $this->getServiceLocator()->get('Application\Service\Categoria');
        $result = $serviceCategoria->delete($id);

        if ($result) {
            return $result;
        } else {
            return array('success' => false);
        }
    }
}
