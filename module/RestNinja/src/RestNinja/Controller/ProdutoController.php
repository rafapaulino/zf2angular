<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RestNinja\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;


class ProdutoController extends AbstractRestfulController
{
    public function getList()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $em->getRepository('Application\Entity\Produto')->findAll();
        return $data;
    }

    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $em->getRepository('Application\Entity\Produto')->find($id);
        return $data;
    }

    public function create($data)
    {
        $serviceProduto = $this->getServiceLocator()->get('Application\Service\Produto');
        $p = $serviceProduto->insert($data);

        if ($p) {
            return $p;
        } else {
            return array('success' => false);
        }
    }

    public function update($id, $data)
    {
        $serviceProduto = $this->getServiceLocator()->get('Application\Service\Produto');
        $p = $serviceProduto->update($data);

        if ($p) {
            return $p;
        } else {
            return array('success' => false);
        }
    }

    public function delete($id)
    {
        $serviceProduto = $this->getServiceLocator()->get('Application\Service\Produto');
        $result = $serviceProduto->delete($id);

        if ($result) {
            return $result;
        } else {
            return array('success' => false);
        }
    }
}
