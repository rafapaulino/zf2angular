<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Entity\Produto;

class LoadProduto extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categoriaLivros = $this->getReference('categoria-livros');
        $categoriaSuplementos = $this->getReference('categoria-suplementos');

        $prod = new Produto();
        $prod->setNome("Orientação a Objetos")
             ->setCategoria($categoriaLivros)
             ->setDescricao("Descrição do livro");
        $manager->persist($prod);

        $prod2 = new Produto();
        $prod2->setNome("Maltodextrina")
            ->setCategoria($categoriaSuplementos)
            ->setDescricao("Maltodextrina");
        $manager->persist($prod2);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 20;
    }

} 