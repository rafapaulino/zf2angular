'use strict';
angular.module('myApp.services',['ngResource'])
    .factory('CategoriasSrv', ['$resource' ,function($resource){
            return $resource(
                '/zf2angular/public/api/categoria/:id', {
                    id: '@id'
                },
                {
                    update: {
                        method: 'PUT'
                    }
                }
            );
        }]
    )
    .factory('ProdutosSrv', ['$resource' ,function($resource){
        return $resource(
            '/zf2angular/public/api/produto/:id', {
                id: '@id'
            },
            {
                update: {
                    method: 'PUT'
                }
            }
        );
    }]
    );