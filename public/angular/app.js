'use strict';
//cria a aplicacao
var myApp = angular.module('myApp',['ngRoute','myApp.controllers']);
//cria as configuracoes das rotas
myApp.config(
    [
        '$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'angular/templates/categorias.html',
                    controller: 'CategoriasCtrl'
                })
                .when('/categorias', {
                    templateUrl: 'angular/templates/categorias.html',
                    controller: 'CategoriasCtrl'
                })
                .when('/categorias/novo', {
                    templateUrl: 'angular/templates/categorias_novo.html',
                    controller: 'CategoriasCtrl'
                })
                .when('/categorias/editar/:id', {
                    templateUrl: 'angular/templates/categorias_editar.html',
                    controller: 'CategoriasCtrl'
                })
                .when('/produtos', {
                    templateUrl: 'angular/templates/produtos.html',
                    controller: 'ProdutosCtrl'
                })
                .when('/produtos/novo', {
                    templateUrl: 'angular/templates/produtos_novo.html',
                    controller: 'ProdutosCtrl'
                })
                .when('/produtos/editar/:id', {
                    templateUrl: 'angular/templates/produtos_editar.html',
                    controller: 'ProdutosCtrl'
                })
            ;
        }
    ]
);

