'use strict';
angular.module('myApp.controllers',['ngRoute','myApp.services'])
    .controller('CategoriasCtrl',
        ['$scope','CategoriasSrv','$location','$routeParams',
            function($scope, CategoriasSrv, $location, $routeParams) {

                $scope.load = function() {
                    //query vem do angular resource
                    $scope.registros = CategoriasSrv.query();
                }

                $scope.get = function() {
                    $scope.item = CategoriasSrv.get({id: $routeParams.id});
                }

                $scope.add = function(item) {
                    $scope.result = CategoriasSrv.save(
                        {},
                        item,
                        function(data, status, header, config) {
                            $location.path('/categorias/');
                        },
                        function(data, status, header, config) {
                            alertify.alert().set({
                                'message':'Erro ao inserir o registro!',
                                'title': 'Atenção!',
                                'label':'Ok, entendi!'
                            }).show();
                        }
                    );
                }

                $scope.edit = function(item) {
                    $scope.result = CategoriasSrv.update(
                        {id: $routeParams.id},
                        item,
                        function(data, status, header, config) {
                            $location.path('/categorias/');
                        },
                        function(data, status, header, config) {
                            alertify.alert().set({
                                'message':'Erro ao editar o registro!',
                                'title': 'Atenção!',
                                'label':'Ok, entendi!'
                            }).show();
                        }
                    );
                }

                $scope.delete = function(id) {
                    //get the closable setting value.
                    var closable = alertify.dialog('confirm').setting('closable');
                    //grab the dialog instance and set multiple settings at once.
                    alertify.dialog('confirm')
                        .set({
                            'labels':{ok:'Deletar', cancel:'Cancelar'},
                            'title': 'Atenção!',
                            'message': 'Deseja Realmente Excluir este registro?',
                            'onok': function(){
                                CategoriasSrv.remove(
                                    {id: id},
                                    {},
                                    function(data, status, header, config) {
                                        $scope.load();
                                    },
                                    function(data, status, header, config) {
                                        alertify.alert().set({
                                            'message':'Erro ao deletar o registro!',
                                            'title': 'Atenção!',
                                            'label':'Ok, entendi!'
                                        }).show();
                                    }
                                )
                            }
                        }).show();
                }
            }
        ]
    )
    .controller('ProdutosCtrl',
        ['$scope','ProdutosSrv','CategoriasSrv','$location','$routeParams',
            function($scope, ProdutosSrv, CategoriasSrv, $location, $routeParams) {

                $scope.load = function() {
                    //query vem do angular resource
                    $scope.registros = ProdutosSrv.query();
                }

                $scope.getCategorias = function() {
                    $scope.categorias = CategoriasSrv.query();
                }

                $scope.get = function() {
                    $scope.item = ProdutosSrv.get({id: $routeParams.id});
                    $scope.getCategorias();
                }

                $scope.add = function(item) {
                    $scope.result = ProdutosSrv.save(
                        {},
                        item,
                        function(data, status, header, config) {
                            $location.path('/produtos/');
                        },
                        function(data, status, header, config) {
                            alertify.alert().set({
                                'message':'Erro ao inserir o registro!',
                                'title': 'Atenção!',
                                'label':'Ok, entendi!'
                            }).show();
                        }
                    );
                }

                $scope.edit = function(item) {
                    $scope.result = ProdutosSrv.update(
                        {id: $routeParams.id},
                        item,
                        function(data, status, header, config) {
                            $location.path('/produtos/');
                        },
                        function(data, status, header, config) {
                            alertify.alert().set({
                                'message':'Erro ao editar o registro!',
                                'title': 'Atenção!',
                                'label':'Ok, entendi!'
                            }).show();
                        }
                    );
                }

                $scope.delete = function(id) {
                    //get the closable setting value.
                    var closable = alertify.dialog('confirm').setting('closable');
                    //grab the dialog instance and set multiple settings at once.
                    alertify.dialog('confirm')
                        .set({
                            'labels':{ok:'Deletar', cancel:'Cancelar'},
                            'title': 'Atenção!',
                            'message': 'Deseja Realmente Excluir este registro?',
                            'onok': function(){
                                ProdutosSrv.remove(
                                    {id: id},
                                    {},
                                    function(data, status, header, config) {
                                        $scope.load();
                                    },
                                    function(data, status, header, config) {
                                        alertify.alert().set({
                                            'message':'Erro ao deletar o registro!',
                                            'title': 'Atenção!',
                                            'label':'Ok, entendi!'
                                        }).show();
                                    }
                                )
                            }
                    }).show();
                }
            }
        ]
    );